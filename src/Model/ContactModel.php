<?php
namespace App\Model;
use Core\App;
use Core\Kernel\AbstractModel;

class ContactModel extends AbstractModel{
    protected static $table = 'contact';

    public static function getTable(){
        return self::$table;
    }

    private $id;

    public function getId(){
        return $this->id;
    }

    private $sujet;

    public function getSujet(){
        return $this->sujet;
    }

    private $email;

    public function getEmail(){
        return $this->email;
    }

    private $message;

    public function getMessage(){
        return $this->message;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (email, sujet, message, created_at) VALUES (?,?,?,NOW())",
            array($post['email'], $post['sujet'], $post['message'])
        );
    }

    public static function getAllContactOrderBy($column = 'email', $order ='ASC'){
        return App::getDatabase()->query("SELECT * FROM " .self::getTable() . " ORDER BY $column $order",get_called_class());
    }

}