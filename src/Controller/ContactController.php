<?php
namespace App\Controller;
use App\Model\ContactModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class ContactController extends AbstractController{
        public function index(){
            $contacts = new ContactModel();
            $contacta = $contacts->getAllContactOrderBy();
            $this->render('app.contact.liste', array(
                'contacts' => $contacta,
            ), 'contact');
        }

        public function add(){
            $errors = [];
            if (!empty($_POST['submitted'])){
                //Faille XSS
                $post = $this->cleanXss($_POST);
                //Validation
                $validation = new Validation();
                $errors['email'] = $validation->emailValid($post['email']);
                $errors['sujet'] = $validation->textValid($post['sujet'], 'sujet',5, 500);
                $errors['message'] = $validation->textValid($post['message'], 'message',5, 500);
                if($validation->isValid($errors))  {
                    ContactModel::insert($post);
                    // Message flash
                    $this->addFlash('success', 'Merci pour l\'ajout du contact !');
                    // redirection
                    $this->redirect('listing');
                }
            }
            $form = new Form($errors);
            $this->render('app.contact.index', [
                    'form' => $form,
            ], 'contact');
        }
}