<form action="" method="post" novalidate style="display: flex; align-items: center; flex-direction: column " class="wrap">
    <?php echo $form->label('email');?>
    <?php echo $form->input('email', 'email');?>
    <?php echo $form->error('email');?>

    <?php echo $form->label('Sujet');?>
    <?php echo $form->input('sujet', 'text');?>
    <?php echo $form->error('sujet');?>

    <?php echo $form->label('Message');?>
    <?php echo $form->textarea('message');?>
    <?php echo $form->error('message');?>

    <?php echo $form->submit('submitted', $textButton)?>
</form>