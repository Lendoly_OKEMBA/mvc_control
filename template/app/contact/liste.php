<table>
    <thead>
    <tr>
        <th>Email</th>
        <th>Sujet</th>
        <th>Messages</th>
        <th>Date de création</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($contacts as $contact){ ?>
        <tr>
            <td>
                <?php   echo $contact->email; ?>
            </td>
            <td>
                <?php   echo $contact->sujet; ?>
            </td>
            <td>
                <?php   echo $contact->message; ?>
            </td>
            <td>
                <?php   echo date('d/M/Y'); ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <th>Email</th>
        <th>Sujet</th>
        <th>Messages</th>
        <th>Date de création</th>
    </tr>
    </tfoot>
</table>