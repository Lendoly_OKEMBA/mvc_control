<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact</title>
    <?php echo $view->add_webpack_style('contact'); ?>
  </head>
  <body>
  <?php // $view->dump($view->getFlash()) ?>
    <header id="masthead">
      <nav>
          <ul>
              <li><a href="<?= $view->path(''); ?>">Home</a></li>
              <li><a href="<?= $view->path('listing'); ?>">listing</a></li>
          </ul>
      </nav>
    </header>

    <div class="container">
        <?= $content; ?>
    </div>

    <footer id="colophon">
        <div class="wrap">
            <p>MVC 6 - Contact</p>
        </div>
    </footer>
  <?php echo $view->add_webpack_script('contact'); ?>
  </body>
</html>
